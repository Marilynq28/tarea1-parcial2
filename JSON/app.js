
const consultarData = (nombre, apellido) => {
    const xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            let respuesta = JSON.parse(xhttp.responseText);
            let estudiantes = respuesta.estudiantes;
            estudiantes.map(estudiante => {
                if (nombre === estudiante.cedula && nombres === estudiante.cedula) {
                    recibirDato(estudiante)
                }
            })
            // let salida = '';
            // for (let i = 0; i < personas.length; i++) {
            //     salida += `<li>${personas[i].nombre} su nickname e</li>`;

            // }
            // document.getElementById('personas')
            //     .innerHTML = salida;

        }
    }
    xhttp.open("GET", "estudiantes.json", true);
    xhttp.send();
}



// Funcion para recibir los el nombre del estudiante para filtar a solo un objeto
const recibirDato = (estudiante) => {

    const { cedula, nombres, correo, direccion, paralelo, semestre, telefono } = estudiante

    document.getElementById('datos').innerHTML = `
    <div class="dato-foto">
        <div class="fondo-icon">
            <span class="material-icons">
                person
            </span>
        </div>
    </div>
    <div class="datos">
        <h3 class="titulo">${cedula} ${nombres}</h3>
        <hr>
        <h4 class="subtitulo">correo :<span class="no-capitalizado"> ${correo}  </span></h4>
        <h4 class="subtitulo">telefono :<span> ${telefono}</span></h4>
        <h4 class="subtitulo">direccion :<span> ${direccion} </span></h4>
        <hr>
        <h4 class="subtitulo">semestre :<span> ${semestre}  </span></h4>
        <h4 class="subtitulo">paralelo :<span> ${paralelo} </span></h4>
    </div>
    `
}

const mostrarError = () => {
    document.getElementById('datos').innerHTML = '<p>hola</p>'
}