
const conectarXMLParaTabla = () => {
    const xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            let docXML = this.responseXML;
            let estudiante = docXML.getElementsByTagName("estudiante")
            cargarXML(estudiante)
        }
    }
    xmlhttp.open("GET", './estudiantes.xml', true);
    xmlhttp.send();

    // xmlhttp.open("GET", "./estudiantes.xml", true);
    // xmlhttp.send();
    // const xmlDoc = xmlhttp.responseXML;
    // console.log(xmlDoc.getElementsByTagName("nombres")[0].childNodes[0].nodeValue);
}

const conectarXMLParaIndividual = () => {
    const xmlhttp = new XMLHttpRequest();
    var cod = document.getElementById("estudiante").value;
    xmlhttp.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            /* console.log(this); */
            let docXML = this.responseXML;
/*             console.log(docXML);
 */            let estudiante = docXML.getElementsByTagName("estudiante")
            /* cargarXML(this) */
            for (let i = 0; i < estudiante.length; i++) {
                let nombresXML = estudiante[i].getElementsByTagName("nombres")[0].textContent
                let apellidosXML = estudiante[i].getElementsByTagName("cedula")[0].textContent
                let completo = `${nombresXML} ${apellidosXML}`;
                console.log(completo);
                if (completo == cod) {
                    console.log(estudiante[i]);
                    cargarXML(estudiante[i])
                }
            }
        }
    }
    xmlhttp.open("GET", './estudiantes.xml', true);
    xmlhttp.send();
}



const cargarXML = (estudiante) => {
    console.log(estudiante);
    console.log(estudiante.length);

    /* console.log(xml);
    let docXML = xml.responseXML; */
    let tabla = `<tr>
    <th class="borde-de-ar"><span class="material-icons">
    format_list_numbered
    </span></th>
    <th>Nombres</th>
    <th>cedula</th>
    <th>Semestre</th>
    <th>Paralelo</th>
    <th>Direccion</th>
    <th>Telefono</th>
    <th class="borde-iz-ar">Correo</th>
</tr>`;
    if (typeof (estudiante.length) == "undefined") {
        tabla += `
        <tr class="borde-interno hover">
            <td class="centar-text ">${1}</td>
            <td>${estudiante.getElementsByTagName("nombres")[0].textContent}</td >
            <td>${estudiante.getElementsByTagName("cedula")[0].textContent}</td >
            <td>${estudiante.getElementsByTagName("semestre")[0].textContent}</td>
            <td class="centar-text">${estudiante.getElementsByTagName("paralelo")[0].textContent}</td>
            <td>${estudiante.getElementsByTagName("direccion")[0].textContent}</td>
            <td>${estudiante.getElementsByTagName("telefono")[0].textContent}</td>
            <td class="no-capitalizar ">${estudiante.getElementsByTagName("correo")[0].textContent}</td>
        </tr>
    `;
    }
    //console.log(estudiante.getElementsByTagName("nombres")[0].textContent);
/*     let estudiante = docXML.getElementsByTagName("estudiante")
 */    for (let i = 0; i < estudiante.length; i++) {
        tabla += `
            <tr class="borde-interno hover">
                <td class="centar-text ">${i + 1}</td>
                <td>${estudiante[i].getElementsByTagName("nombres")[0].textContent}</td >
                <td>${estudiante[i].getElementsByTagName("apellidos")[0].textContent}</td >
                <td>${estudiante[i].getElementsByTagName("semestre")[0].textContent}</td>
                <td class="centar-text">${estudiante[i].getElementsByTagName("paralelo")[0].textContent}</td>
                <td>${estudiante[i].getElementsByTagName("direccion")[0].textContent}</td>
                <td>${estudiante[i].getElementsByTagName("telefono")[0].textContent}</td>
                <td class="no-capitalizar ">${estudiante[i].getElementsByTagName("correo")[0].textContent}</td>
            </tr>
        `;
    }
    document.getElementById("pr").innerHTML = tabla;
}
